.PHONY: bin web clean

CXX=g++
RM=rm -f
CXXFLAGS=-Iinclude -Ilibs/game-engine/include -Ilibs/game-engine/libs -std=c++20 -Wall -Wextra -Werror
LDFLAGS=
SOURCES=$(shell find libs/game-engine/src -name "*.cpp") $(shell find src -name "*.cpp")
HEADERS=$(shell find include -name "*.hpp")
OBJS=$(patsubst %.cpp,%.o,$(SOURCES))
OUT=bin

web: CXX=emcc
web: CXXFLAGS += -s USE_SDL=2
web: LDFLAGS += -s USE_SDL=2 -s MAX_WEBGL_VERSION=2
web: $(OUT)/index.html

bin: LDFLAGS += -lSDL2 -lGL -DDEBUG -g
bin: $(OUT)/main

clean:
	$(RM) -r *.o
	$(RM) $(OUT)/index.html
	$(RM) $(OUT)/main

$(OUT)/index.html: $(OBJS)
	@mkdir -p ${dir $@}
	$(CXX) $(OBJS) $(LDFLAGS) -o $@

$(OUT)/main: $(OBJS)
	@mkdir -p ${dir $@}
	$(CXX) $(OBJS) $(LDFLAGS) -o $@

%.o: %.cpp $(HEADERS)
	$(CXX) -c $< $(CXXFLAGS) -o $@
