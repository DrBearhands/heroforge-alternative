bl_info = {
    "name": "Export Stanford Triangle Format (.ply) with vertex group weights",
    "author": "Oscar Leijendekker",
    "version": (0, 1, 0),
    "blender": (2, 83, 5),
    "location": "File > Export > PLY with vertex groups",
    "description": "The script exports blender geometry to ASCII PLY format where vertex elements have additional group and weight fields. Useful for bone animations.",
    "warning": "Under construction",
    "category": "Import-Export"
}

import bpy
import mathutils

from bpy.props import (
    #BoolProperty,
    #EnumProperty,
    StringProperty,
)

def register():
    bpy.utils.register_class(PLY_exporter)

def unregister():
    bpy.utils.unregister_class(PLY_exporter)

class PLY_exporter(bpy.types.Operator):
    bl_idname = "export.ply_vertex_groups"
    bl_label = "Export PLY with vertex groups"

    filepath: StringProperty(subtype='FILE_PATH')

    def execute(self, context):
        filePath = bpy.path.ensure_ext(self.filepath, ".ply")
        print(self.filepath)
        scene = bpy.context.scene
        for obj in scene.objects:
            if obj.type == 'MESH':
                armature = obj.find_armature()
                if armature != None:
                    bone_dict = _create_bone_dict(obj)
                    output = "\n".join(
                        [ "ply"
                        , "format ascii 1.0"
                        , "element vertex " + str(len(obj.data.vertices))
                        , "property float x"
                        , "property float y"
                        , "property float z"
                        , "property list uchar uint group"
                        , "property list uchar float weight"
                        , "element face " + str(len(obj.data.polygons))
                        , "property list uchar uint vertex_indices"
                        , "end_header"
                        ]
                        + [_vertexLine(vert, obj.vertex_groups, bone_dict) for vert in obj.data.vertices]
                        + [_polygonLine(polygon) for polygon in obj.data.polygons]
                    )
                    with open(self.filepath, 'w') as file:
                        file.write(output)

    def invoke(self, context, event):
        if not self.filepath:
            self.filepath = bpy.path.ensure_ext(bpy.data.filepath, ".ply")
        WindowManager = context.window_manager
        WindowManager.fileselect_add(self)
        return {'RUNNING_MODAL'}


def _unzip(iter):
    a_list = []
    b_list = []
    for a, b in iter:
        a_list.append(a)
        b_list.append(b)
    return (a_list, b_list)

def _group_iter(vert, vertex_groups, bone_dict):
    for group in vert.groups:
        bone_index = bone_dict[vertex_groups[group.group].name]
        yield(bone_index, group.weight)

def _vertexLine(vert, groups, bone_dict):
    nGroups = str(len(vert.groups))
    bone_indices, weights = _unzip(_group_iter(vert, groups, bone_dict))
    return " ".join([str(f) for f in vert.co]
        + [nGroups]
        + [str(bone_idx) for bone_idx in bone_indices]
        + [nGroups]
        + [str(weight) for weight in weights])

def _polygonLine(polygon):
    return " ".join([str(len(polygon.vertices))] + [str(i) for i in polygon.vertices])

def _create_bone_dict(obj):
    armature = obj.find_armature()
    return {pose_bone.bone.name: index for pose_bone, index in zip(armature.pose.bones, range(len(armature.pose.bones)))}


if __name__ == "__main__":
    register()
