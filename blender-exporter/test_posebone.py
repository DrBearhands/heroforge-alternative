import bpy
import mathutils

#TODO: apply transform / modifiers to mesh

def posebone_weight(vert, obj, pose_dict):
    for group in vert.groups:
        group_name = obj.vertex_groups[group.group].name
        yield (pose_dict[group_name], group.weight)

# still bad
def apply_pose(obj):
    print(obj.name)
    armature = obj.find_armature()
    pose_dict = {pose_bone.bone.name: pose_bone for pose_bone in armature.pose.bones}
    for vert in obj.data.vertices:
        total_weight = 0
        position_acc = mathutils.Vector((0,0,0))
        for (pose_bone, weight) in posebone_weight(vert, obj, pose_dict):
            mat = pose_bone.matrix @ pose_bone.bone.matrix_local.inverted()
            total_weight += weight
            position_acc += (mat @ vert.co) * weight
        if total_weight != 0:
            vert.co = position_acc / total_weight

apply_pose(bpy.context.active_object)
