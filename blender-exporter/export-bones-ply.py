import bpy
import bmesh

# from https://blender.stackexchange.com/questions/65129/how-do-i-create-a-script-for-geometry-i-create
def makeCubeAt(mat, name):
    bpyscene = bpy.context.scene
    mesh = bpy.data.meshes.new('Basic_Cube')
    basic_cube = bpy.data.objects.new(name + "_joint", mesh)
    bm = bmesh.new()
    bmesh.ops.create_cube(bm, size=0.1)
    bm.to_mesh(mesh)
    bm.free()
    basic_cube.matrix_world = mat
    bpy.context.collection.objects.link(basic_cube)


def matrixIter(mat):
    for v in mat:
        for s in v:
            yield s

def final_mat(pose_bone):
    mat = pose_bone.matrix
    while pose_bone.parent != None:
        pose_bone = pose_bone.parent
        mat = pose_bone.matrix @ mat
    return mat

def boneLine(pose_bone):
    #pose_bone.bone.matrix_local : position of the bone w.r.t. armature
    #pose_bone.matrix : position of the posebone w.r.t. armature
    boneTransform = pose_bone.matrix @ pose_bone.bone.matrix_local.inverted()
    makeCubeAt(boneTransform, pose_bone.bone.name)
    return " ".join([str(s) for s in matrixIter(boneTransform)])

def export_bones():
    scene = bpy.context.scene
    for obj in scene.objects:
        if obj.type == 'MESH':
            armature = obj.find_armature()
            if armature != None:
                output = "\n".join(
                    [ "ply"
                    , "format ascii 1.0"
                    , "element bone_transform " + str(len(armature.data.bones))
                    , "property float a00"
                    , "property float a01"
                    , "property float a02"
                    , "property float a03"
                    , "property float a10"
                    , "property float a11"
                    , "property float a12"
                    , "property float a13"
                    , "property float a20"
                    , "property float a21"
                    , "property float a22"
                    , "property float a23"
                    , "property float a30"
                    , "property float a31"
                    , "property float a32"
                    , "property float a33"
                    , "end_header"
                    ]
                    + [boneLine(poseBone) for poseBone in armature.pose.bones]
                )
                with open("workspace/heroforge-alternative/models/test-bones.ply", 'w') as file:
                    file.write(output)

export_bones()
