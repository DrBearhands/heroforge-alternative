# Hero forge alternative

Like hero forge but for armies:
- Prices per "item" rather than per unit, allows printing many more of similar units for a reasonable price
  - Attach marketplace for 3rd party sellers
- Different set of poses more fit for armies
- Import own 3D files to attach to model
- Digitally paint model / color preview
- Generate permutations of one unit (poses/heads/etc)
- Pack / sell multiple versions / STL files of the same item / bit to have some variation
- Cloth "animation"?
- more mounts / mounted poses?
- generate build plate for multiple models at once
- more conversion/kitbash style options
  - remove parts of models (boolean subtract operation)
  - Add weirder feature to make something like e.g. plague marines? Would a spore-like editor work?


## What does an MVP look like
Simple human model, can be set in 2-3 poses and have static items added to it.
Maybe allow setting different colors.


### Information needed for mesh
- Grouping for quick color scheme tests
- Skinning for attaching to bones

STL only has vertices
PLY?
obj not really relevant but could import
DAE is rather complicated
